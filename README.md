# FlowStock (Inventory Administrator)

![FlowStock Logo](/static/brand/Banner%20FlowStock%20Project.png)

FlowStock es una aplicación para administrar inventarios de manera fácil, eficiente y segura. La filosofía del proyecto es hacer que la administración de inventarios sea lo más accesible posible y proporcionar la mejor experiencia para el usuario.

## Características Principales

- Registro y autenticación de usuarios sin contraseña, utilizando encriptación de datos en el indexDB localmente.
- Cifrado del 100% de la información del usuario, incluido el inventario.
- Funcionalidad para solicitar y recibir copias cifradas del inventario por correo electrónico.
- Generación de informes y gráficos personalizados sobre el flujo de inventario.
- Desarrollado con SvelteKit y varias librerías para cifrado de datos, envío de correos electrónicos, generación de PDF y autenticación JWT.

## Enlaces Útiles

- [Informacion del proyecto en Notion]([url-de-la-documentacion-en-notion](https://periwinkle-health-a6e.notion.site/FlowStock-Desarrollo-389a0f211e82443aa4744d6ad5286ad1))
- [Repositorio en GitHub](https://gitlab.com/jeisonwee/flowstock-inv-admin)

## Instalación

Para instalar y ejecutar el proyecto localmente, sigue estos pasos:

1. Clona el repositorio desde GitHub:

   ```bash
   git clone https://github.com/tu-usuario/flowstock.git
   ```

2. Instala las dependencias usando [pnpm](https://pnpm.io/):

   ```bash
   cd flowstock
   pnpm install
   ```

3. Ejecuta el proyecto en modo de desarrollo:

   ```bash
   pnpm run dev --host
   ```

4. Accede a la aplicación desde tu navegador web en `http://localhost:3000`.

## Uso

Después de instalar y ejecutar el proyecto, puedes acceder a la aplicación desde tu navegador web y comenzar a utilizar las funcionalidades de FlowStock para administrar tu inventario.

## Contribución

¡Estamos abiertos a contribuciones! Si deseas contribuir al proyecto, sigue estos pasos:

1. Abre un issue para discutir tus ideas o informar sobre errores.
2. Realiza cambios en una rama separada y abre un pull request.
3. Nosotros revisaremos y discutiremos tus cambios antes de fusionarlos con el repositorio principal.

## Licencia

Este proyecto está licenciado bajo la [Licencia Apache 2.0](LICENSE).

## Contacto

Si tienes preguntas o comentarios sobre el proyecto, no dudes en contactarme:

- Correo Electrónico: <jeissonwee.dev@gmail.com>
- Twitter: [![Twitter](https://img.shields.io/twitter/follow/3Jeissonwee?style=social)](https://twitter.com/3Jeissonwee)
- LinkedIn: [![LinkedIn](https://img.shields.io/badge/LinkedIn-Connect-blue)](https://www.linkedin.com/in/jeisson-le%C3%B3n-53b05a1a8/)
- Instagram: [![Instagram](https://img.shields.io/badge/Instagram-Follow-purple)](https://www.instagram.com/jeissonwee/)
- GitLab: [![GitLab](https://img.shields.io/badge/GitLab-Follow-orange)](https://gitlab.com/jeissonwee.dev)

## Capturas de Pantalla

*(Agrega aquí capturas de pantalla de tu proyecto en acción)*
