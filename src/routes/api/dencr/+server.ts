import { config } from "dotenv";
import { type RequestEvent } from "@sveltejs/kit";
import CryptoJS from "crypto-js";

// Cargar variables de entorno desde un archivo .env
config();
const dev = process.env.NODE_ENV === "production";
const jwtKey = process.env.JWT_SECRET || null;

function returnError(status: number, message: string) {
  return new Response(message, { status });
}

function decryFieldData(field: string) {
  try {
    const decryDataBytes = CryptoJS.AES.decrypt(field, jwtKey);
    const decryData = JSON.parse(decryDataBytes.toString(CryptoJS.enc.Utf8));

    if (decryData === '' || decryData === undefined || decryData === null) {
      throw new Error("That data isn't valid fox");
    }

    return decryData;
  } catch (err) {
    console.log("> Decriptacion:", err);
    throw new Error(err);
  }
}

async function valDecry(data: string) {
  try {
    if (typeof data !== "string") {
      throw new Error("That data isn't valid bitch");
    }

    // Verifica si el dato encriptado tiene al menos 6 caracteres
    if (data.length < 10) {
      throw new Error("What u doing, really?");
    }

    return decryFieldData(data);
  } catch (err) {
    console.log("> Validacion:", err);
    throw new Error(err);
  }
}

export const POST = async (event: RequestEvent) => {
  try {
    const body = await event.request.json();

    // Metodo de desencriptacion general de datos
    if (body.method === 2) {
      const { u, a, i, e } = body.data;

      console.log('\nMethod 2 used -------------------------')
      console.log('Id:', i);
      console.log('User:', u);
      console.log('Email:', e);
      console.log('Auto contri:', a);

      if (!u || !a || !i || !e) {
        return returnError(
          400,
          JSON.stringify({ error: "Invalid request pirobo" })
        );
      }

      try {
        await valDecry(i);
        const uVal = await valDecry(u);
        const eVal = await valDecry(e);
        const aVal = await valDecry(a);

        const allVal = {
          u: uVal,
          e: eVal,
          a: aVal,
        };

        console.log(JSON.stringify(allVal))

        return new Response(JSON.stringify(allVal), { status: 200 });
      } catch (err) {
        return new Response("Data error sucker", { status: 500 });
      }
    } 

    // Metodo de desencriptacion especifica de campos
    if (body.method == 1) {
      const { val } = body.data;

      console.log('\nMethod 1 used -----------------------')

      if (!val) {
        return returnError(
          400,
          JSON.stringify({ error: "Invalid camp pirobo" })
        );
      }

      if (val) {
        try {
          const valDecryted = await valDecry(val);

          return new Response(JSON.stringify({ val: valDecryted }), {
            status: 200,
          });
        } catch (err) {
          return new Response("Data error sucker", { status: 500 });
        }
      }
    }

    throw new Error("Invalid Method Gonorrea");
  } catch (err) {
    console.log(err);
    return returnError(400, "Error careverga !");
  }
};
