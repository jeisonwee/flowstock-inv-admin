/* -------- Este es un endpoint de prueba -------- */

import { json } from '@sveltejs/kit';

export async function GET(event: any) {
	const companyDetails = {
		name: 'My Company Test Api:v',
		employees: [
			{ name: 'Sara Weletorky', salary: 10000000 },
			{ name: 'Jane Larkin', salary: 42000 },
			{ name: 'Jim Salmon', salary: 38000 }
		],
	};
	// console.log(event.request)

	return json(companyDetails);
}
