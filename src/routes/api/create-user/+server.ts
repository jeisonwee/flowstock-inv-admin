import { RequestEvent } from "@sveltejs/kit";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { config } from "dotenv";

// Cargar variables de entorno desde un archivo .env
config();
const dev = process.env.NODE_ENV === "production";

let db: any = [
  {
    email: "admin@test.com",
    username: "Administrator Sexy",
    password: "Admin123",
    user_id: 1,
    refresh_token: "abcdefg",
  },
];

function returnError(status: number, message: string) {
  return new Response(message, { status });
}

export const POST = async (event: RequestEvent) => {
  const body = await event.request.json();
  console.clear();

  try {
    // Check if request body is valid
    if (!body.email || !body.password || !body.username) {
      return returnError(400, `Invalid request Mazamorra`);
    }
    if (body.username.length < 4 || body.password.length < 6) {
      return returnError(400, "Bad request puto");
    }

    // Check if email or username is already in use
    const existingUser = db.find(
      (user) => user.email === body.email || user.username === body.username
    );
    if (existingUser) {
      console.log(`> El "${body.username}" esta tratando de crearse de nuevo`)
      return returnError(405, "Usuario ya existe maldito pedazo de mierda");
    }

    // Hash the user's password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(body.password, salt);

    // Generate user ID and refresh token
    const user_id = crypto.randomUUID();
    const refresh_token = crypto.randomUUID();

    // Insert the new user into the database
    db.push({
      email: body.email,
      username: body.username,
      password: hashedPassword, 
      user_id,
      refresh_token,
    });

    // Generate JWT token with secret key from environment variable
    const token = jwt.sign(
      {
        username: body.username,
        user_id,
        email: body.email,
      },
      process.env.JWT_SECRET || "",
      {}
    );

    // Set cookies
    const headers = new Headers();
    headers.set(
      "Set-Cookie",
      `refresh_token=${refresh_token}; Max-Age=${
        30 * 24 * 60 * 60
      }; Path=/; ${dev ? 'Secure;' : ''}; HttpOnly; SameSite=strict`
    );
    headers.set(
      "Set-Cookie",
      `token=${token}; Path=/; ${dev ? 'Secure;' : ''} HttpOnly; SameSite=strict`
    );

    return new Response("Listo perra", {
      status: 200,
      headers,
    });
  } catch (error) {
    console.error("Error creating user:", error);
    return new Response("Internal Server Error Guapo", { status: 500 });
  }
};
