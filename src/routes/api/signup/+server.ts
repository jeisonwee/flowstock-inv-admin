import { config } from "dotenv";
import { RequestEvent } from "@sveltejs/kit";
import jwt from "jsonwebtoken";
import CryptoJS from "crypto-js";

// Cargar variables de entorno desde un archivo .env
config();
const dev = process.env.NODE_ENV === "production";
const jwtKey = process.env.JWT_SECRET || null;

function returnError(status: number, message: string) {
  return new Response(message, { status });
}

export const POST = async (event: RequestEvent) => {
  const body = await event.request.json();
  console.clear();
  console.log(body);

  // Check if request body is valid
  function verifyBool(variable: boolean) {
    // Comprobamos si la variable está definida y no es null
    if (typeof variable !== "undefined" && variable !== null) {
      return typeof variable === "boolean";
    } else {
      return false;
    }
  }

  try {
    if (!body.email || !body.username || !verifyBool(body.acceptContri)) {
      return returnError(400, `Invalid request Mazamorra`);
    }
    if (body.username.length < 4 || body.email.length < 6) {
      return returnError(400, "Bad request puto");
    }

    // Encriptacion de campo
    const encryptField = (field: any) => {
      const fieldString = JSON.stringify(field);
      const encryptedField = CryptoJS.AES.encrypt(
        fieldString,
        jwtKey
      ).toString();
      return encryptedField;
    };

    // Identificador unico 
    const user_id = crypto.randomUUID();

    const formData = {
      idUser: encryptField(user_id),
      email: encryptField(body.email),
      username: encryptField(body.username),
      acceptContri: encryptField(body.acceptContri),
    };
    console.log(formData);

    // Generate JWT token with secret key from environment variable
    const token = jwt.sign(user_id, jwtKey, {});

    // Set cookies
    const headers = new Headers();
    headers.set(
      "Set-Cookie",
      `token=${token}; Path=/; ${
        dev ? "Secure;" : ""
      } HttpOnly; SameSite=strict`
    );

    return new Response(JSON.stringify(formData), {
      status: 200,
      headers,
    });
  } catch (error) {
    console.error("Error creating user:", error);
    return new Response("Internal Server Error Pirobo", { status: 500 });
  }
};
