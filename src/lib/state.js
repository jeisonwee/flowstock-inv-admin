import { writable } from "svelte/store";

const userStore = writable({
  username: null,
  email: null,
  accContri: null,
  loading: true,
  error: false,
});

export const setUserData = ({ u, e, a, l, error }) => {
  userStore.update((user) => {
    user.username = u || null;
    user.email = e || null;
    user.accContri = a || null;
    user.loading = l || false;
    user.error = error || false;
    return user;
  });
};

export let getUserData = () => {
  return userStore;
};
