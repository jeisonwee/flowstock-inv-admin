import { writable } from 'svelte/store';

// Estado de insercion en el campo de Header
export const headerContent = writable(null);

// Estado o conexion para la comunicacion entre el elemento insertado y el contexto del componente quien lo inserto, para que este componente insertado pueda comunicarse con el componente insertor
export const connContext = writable({})