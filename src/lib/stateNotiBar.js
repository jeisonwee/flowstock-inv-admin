/* 
    SISTEMA DE MANEJO DE ESTADOS PARA LAS NOTIFICACIONES Y ELEMENTOS ESTATICOS
    By: Jeisson Leon
*/

let state = {};

// Método para agregar un objeto al estado
export function addItem(item) {
  state = item; // Actualizamos el estado con el nuevo objeto
  sessionStorage.setItem("state", JSON.stringify(state)); // Actualizamos sessionStorage con el nuevo estado
}

// Método para reiniciar el estado
export function resetState() {
  state = {};
  sessionStorage.removeItem("state");
}

// Función para esperar cambios en el estado
export function waitForStateChange(callback) {
  let previousState = JSON.stringify(state); // Estado inicial
  
  const checkState = () => {
    const currentState = state; // Nuevo estado se verifica

    if (currentState !== previousState) {
      callback(currentState); // Llamamos al callback con el estado actualizado
      previousState = currentState; // Actualizamos el estado anterior con el actual
    }

    setTimeout(checkState, 1000); // Comprobamos nuevamente cada 1 segundo
  };

  checkState(); // Comenzamos a comprobar el estado
}

let stateNoti = [];

export function addNoti(item) {
  stateNoti.push(item);
  sessionStorage.setItem("stateNoti", JSON.stringify(stateNoti));
}

export function resetNoti() {
  stateNoti = [];
  sessionStorage.removeItem("stateNoti");
}

export function waitForStateNoti() {
  let previousState = JSON.stringify(stateNoti); // Estado inicial

  const checkState = () => {
    const currentState = JSON.parse(
      sessionStorage.getItem("stateNoti") || "[]"
    ); // Nuevo estado
    const currentStateString = JSON.stringify(currentState);

    if (currentStateString !== previousState) {
      callback(currentState); // Llamamos al callback con el estado actualizado
      previousState = currentStateString; // Actualizamos el estado anterior con el actual
    }

    setTimeout(checkState, 1000); // Comprobamos nuevamente cada 1 segundo
  };

  checkState(); // Comenzamos a comprobar el estado
}
