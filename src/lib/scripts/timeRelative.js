export function obtenerTiempoRelativo(fecha, intervalos) {
  const ahora = new Date();
  const tiempoPasado = ahora - fecha;

  // Convertir el tiempo a segundos
  const segundos = Math.floor(tiempoPasado / 1000);

  // Definir los intervalos de tiempo en segundos
  const intervaloSegundos = {
    año: 31536000,
    mes: 2592000,
    semana: 604800,
    día: 86400,
    hora: 3600,
    minuto: 60,
  };

  // Calcular el tiempo relativo
  for (let intervalo in intervaloSegundos) {
    const cantidad = Math.floor(segundos / intervaloSegundos[intervalo]);
    if (cantidad >= 1) {
      return { cantidad, intervalo: intervalos[intervalo] };
    }
  }

  return { cantidad: 0, intervalo: "minuto" }; // Si la diferencia es inferior a 1 minuto
}
