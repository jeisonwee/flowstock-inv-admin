import { config } from "dotenv";
import { redirect } from "@sveltejs/kit";
import jwt from "jsonwebtoken";

// Cargar variables de entorno desde un archivo .env
config();
const dev = process.env.NODE_ENV === "production";
const jwtKey = process.env.JWT_SECRET || null;

/** @type {import('@sveltejs/kit').Handle} */
export async function handle({ event, resolve }) {
  const pathname = event.url.pathname;

  // Rutas no protegidas
  const exemptRoutes = ["/help", "/privacy", "/politics", "/test"];
  const exemptRouteSys = ["/login", "/api/signup"];

  // Todas las rutas no protegidas
  const allExemptRoutes = exemptRoutes.concat(exemptRouteSys);

  // Funcion de escepcion para rutas no protegidas
  function isExemptRoute(pathn, exemRoutes) {
    // Verificamos si la ruta está en las rutas protegidas o comienza con algún prefijo
    if (
      exemRoutes.includes(pathn) ||
      exemRoutes.some((prefix) => pathn.startsWith(prefix))
    ) {
      return true;
    } else {
      return false;
    }
  }

  // Rutas protegidas
  if (!isExemptRoute(pathname, allExemptRoutes)) {
    // Validacion de token en cookie
    const { headers } = event.request;
    const cookies = headers.get("cookie") || false;

    if (!cookies) {
      return redirect(302, "/login");
    }

    if (cookies) {
      let token = null;
      token = cookies.split(";").reduce((prev, curr) => {
        const [key, value] = curr.trim().split("=");
        return key === "token" ? value : prev;
      }, "");

      if (!token) {
        return redirect(302, "/login");
      }

      if (token) {
        try {
          jwt.verify(token, jwtKey);
          const response = await resolve(event);
          return response;
        } catch (error) {
          return redirect(302, "/login");
        }
      } else {
        return redirect(302, "/login");
      }
    }

    return redirect(302, "/login");
  }

  // Rutas no protegidas del sistema
  if (isExemptRoute(pathname, exemptRouteSys)) {
    // Bloque de comprobacion de session
    const { headers } = event.request;
    const cookies = headers.get("cookie") || false;

    if (!cookies) {
      return await resolve(event);
    }

    if (cookies) {
      let token = null;
      token = cookies.split(";").reduce((prev, curr) => {
        const [key, value] = curr.trim().split("=");
        return key === "token" ? value : prev;
      }, "");

      if (token) {
        try {
          jwt.verify(token, jwtKey);
        } catch {
          // Borrado de token por pirobo y sapo
          const headers = new Headers();
          headers.set(
            "Set-Cookie",
            `token=; expires=Thu, 01 Jan 1970 00:00:00 GMT; Path=/; ${
              dev ? "Secure;" : ""
            } HttpOnly; SameSite=strict`
          );
          return new Response("Token de mierda", {
            status: 400,
            headers,
          });
        }
        return redirect(302, "/");
      }

      if (!token) {
        const response = await resolve(event);
        return response;
      }
    }
  }

  if (isExemptRoute(pathname, allExemptRoutes)) {
    return await resolve(event);
  }

  return new Response("ERROR BRO, what u doing bitch?", { status: 500 });
}
